<?php
/**
 * WooCommerce hooks and functions.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Remove subcategories from the shop page and tell the Konte theme know that removed.
 */
function konte_addons_remove_shop_subcategories() {
	remove_filter( 'woocommerce_product_loop_start', 'woocommerce_maybe_show_product_subcategories' );
}

add_action( 'woocommerce_before_shop_loop', 'konte_addons_remove_shop_subcategories', 49 );

/**
 * Loads and get size guide instance
 *
 * @return bool|object
 */
function konte_addons_size_guide() {
	if ( ! class_exists( 'WooCommerce' ) ) {
		return false;
	}

	if ( ! class_exists( 'Konte_Addons_WooCommerce_Size_Guide' ) ) {
		require_once 'size-guide.php';
	}

	return Konte_Addons_WooCommerce_Size_Guide::instance();
}

add_action( 'plugins_loaded', 'konte_addons_size_guide' );