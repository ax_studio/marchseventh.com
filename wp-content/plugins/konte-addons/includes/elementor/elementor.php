<?php
namespace KonteAddons;

/**
 * Integrate with Elementor.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Elementor {
	/**
	 * Instance
	 *
	 * @access private
	 */
	private static $_instance = null;

	/**
	 * Instance
	 *
	 * Ensures only one instance of the class is loaded or can be loaded.
	 *
	 * @return Konte_Addons_Elementor An instance of the class.
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Constructor
	 */
	public function __construct() {
		spl_autoload_register( [ $this, 'autoload' ] );

		$this->_includes();
		$this->add_actions();
	}

	/**
	 * Auto load widgets
	 */
	public function autoload( $class ) {
		if ( 0 !== strpos( $class, __NAMESPACE__ ) ) {
			return;
		}

		if ( false === strpos( $class, 'Widget' ) ) {
			return;
		}

		$path = explode( '\\', $class );
		$filename = strtolower( array_pop( $path ) );

		if ( 'Widget' != array_pop( $path ) ) {
			return;
		}

		$filename = str_replace( '_', '-', $filename );
		$filename = KONTE_ADDONS_DIR . 'includes/elementor/widgets/' . $filename . '.php';

		if ( is_readable( $filename ) ) {
			include( $filename );
		}
	}

	/**
	 * Includes files which are not widgets
	 */
	private function _includes() {}

	/**
	 * Hooks to init
	 */
	protected function add_actions() {
		// On Editor - Register WooCommerce frontend hooks before the Editor init.
		// Priority = 5, in order to allow plugins remove/add their wc hooks on init.
		if ( ! empty( $_REQUEST['action'] ) && 'elementor' === $_REQUEST['action'] && is_admin() ) {
			add_action( 'init', [ $this, 'register_wc_hooks' ], 5 );
		}

		add_action( 'elementor/frontend/after_enqueue_styles', [ $this, 'enqueue_styles' ] );
		add_action( 'elementor/frontend/after_register_scripts', [ $this, 'register_scripts' ] );
		add_action( 'elementor/frontend/after_enqueue_scripts', [ $this, 'enqueue_scripts' ] );

		add_action( 'elementor/controls/controls_registered', [ $this, 'init_controls' ] );
		add_action( 'elementor/widgets/widgets_registered', [ $this, 'init_widgets' ] );

		add_action( 'elementor/elements/categories_registered', [ $this, 'add_category' ] );
	}

	/**
	 * Register WC hooks for Elementor editor
	 */
	public function register_wc_hooks() {
		wc()->frontend_includes();
	}

	/**
	 * Register styles
	 */
	public function enqueue_styles() {

	}

	/**
	 * Register styles
	 */
	public function register_scripts() {
		wp_register_script( 'konte-elementor-widgets', KONTE_ADDONS_URL . 'assets/js/elementor-widgets.js', ['jquery', 'elementor-frontend'], KONTE_ADDONS_VER, true );
	}

	/**
	 * Enqueue scripts
	 */
	public function enqueue_scripts() {
		wp_enqueue_script( 'konte-elementor-widgets' );
	}

	/**
	 * Init Controls
	 */
	public function init_controls() {

	}

	/**
	 * Init Widgets
	 */
	public function init_widgets() {
		$widgets_manager = \Elementor\Plugin::instance()->widgets_manager;

		$widgets_manager->register_widget_type( new \KonteAddons\Elementor\Widget\Accordion() );
		$widgets_manager->register_widget_type( new \KonteAddons\Elementor\Widget\Tabs() );
		$widgets_manager->register_widget_type( new \KonteAddons\Elementor\Widget\Icon_Box() );
	}

	/**
	 * Add Konte category
	 */
	public function add_category( $elements_manager ) {
		$elements_manager->add_category(
			'konte',
			[
				'title' => __( 'Konte', 'konte-addons' )
			]
		);
	}
}

Elementor::instance();