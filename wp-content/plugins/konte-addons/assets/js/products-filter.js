jQuery( document ).ready( function ( $ ) {
	'use strict';

	// Select2.
	if ( $.fn.select2 ) {
		$( '.products-filter-widget select' ).each( function() {
			var $select = $( this );

			$select.select2( {
				dir: $( document.body ).hasClass( 'rtl' ) ? 'rtl' : 'ltr',
				width: '100%',
				minimumResultsForSearch: -1,
				dropdownCssClass: 'products-filter-dropdown',
				dropdownParent: $select.parent()
			} );
		} );
	}

	// Remove hidden inputs from price slider.
	$( '.products-filter-widget' ).find( '.widget_price_filter' ).find( 'input[type=hidden]' ).not( '[name=min_price], [name=max_price]' ).remove();

	// Swatch or list selection.
	$( document.body ).on( 'click', '.filter-swatches .swatch, .filter-list .filter-list-item, .filter-checkboxes .filter-checkboxes-item', function ( event ) {
		event.preventDefault();

		var $this = $( this ),
			$filter = $this.closest( '.filter' ),
			$input = $this.parent().next( 'input[type=hidden]' ),
			current = $input.val(),
			value = $this.data( 'value' ),
			form = $( this ).closest( 'form' ).get( 0 ),
			index = -1;

		if ( $filter.hasClass( 'multiple' ) ) {

			current = current ? current.split( ',' ) : [];
			index = current.indexOf( value );
			index = (-1 !== index) ? index : current.indexOf( value.toString() );

			if ( index !== -1 ) {
				current = _.without( current, value );
			} else {
				current.push( value );
			}

			$input.val( current.join( ',' ) );
			$this.toggleClass( 'selected' );

			if ( current.length === 0 ) {
				$input.attr( 'disabled', 'disabled' );
			} else {
				$input.removeAttr( 'disabled' );
			}
		} else {

			if ( $this.hasClass( 'selected' ) ) {
				$this.removeClass( 'selected' );
				$input.val( '' ).attr( 'disabled', 'disabled' );
			} else {
				$this.addClass( 'selected' ).siblings( '.selected' ).removeClass( 'selected' );
				$input.val( value ).removeAttr( 'disabled' );
			}

		}

		$( document.body ).trigger( 'konte_products_filter_change', [form] );
	} );

	// Trigger change form event.
	$( document.body ).on( 'change', '.products-filter-widget input, .products-filter-widget select', function () {
		var form = $( this ).closest( 'form' ).get( 0 );
		$( document.body ).trigger( 'konte_products_filter_change', [form] );
	} ).on( 'price_slider_create', function() {
		var $slider = $( '.products-filter-widget .price_slider.ui-slider' );

		$slider.each( function() {
			var $el = $( this ),
				form = $el.closest( 'form' ).get( 0 ),
				onChange = $el.slider( 'option', 'change' );

			$el.slider( 'option', 'change', function( event, ui ) {
				onChange( event, ui );

				$( document.body ).trigger( 'konte_products_filter_change', [form] );
			} );
		} );
	} );

	// Reset button.
	$( document.body ).on( 'click', '.products-filter-widget .reset-button', function() {
		var $form = $( this ).closest( 'form' );

		$form.get( 0 ).reset();
		$form.find( '.selected' ).removeClass( 'selected' );
		$form.find( ':input' ).not( '[type="button"], [type="submit"], [type="reset"]' )
			.val( '' )
			.trigger( 'change' )
			.filter( '[type="hidden"]' ).prop( 'disabled', true );

		$form.trigger( 'submit' );
		$( document.body ).trigger( 'konte_products_filter_reseted' );
	} );

	// Ajax filter.
	var ajax = null;

	$( document.body ).on( 'submit', '.products-filter-widget form.ajax-filter', function() {
		ajaxSearch( this );

		return false;
	} ).on( 'konte_products_filter_change', function( event, form ) {
		if ( ! $( form ).hasClass( 'instant-filter' ) ) {
			return;
		}

		ajaxSearch( form );
	} );

	// Change the URL after ajax requested.
	$( document.body ).on( 'konte_products_filter_request_success', function ( e, response, url ) {
		if ( '?' === url.slice( -1 ) ) {
			url = url.slice( 0, -1 );
		}

		url = url.replace( /%2C/g, ',' );

		history.pushState( null, '', url );
	} );

	/**
	 * Call ajax request to filter products
	 *
	 * @param DOMObject form
	 */
	function ajaxSearch( form ) {
		var $form = $(form),
			$container = $('ul.products.main-products'),
			$inputs = $form.find(':input:not(:checkbox):not(:button)'),
			params = $inputs.filter(function () { return $(this).val() != ''; }).serializeObject(),
			action = $form.attr('action'),
			separator = action.indexOf('?') !== -1 ? '&' : '?',
			url = action;

		if (params.min_price && params.min_price == $inputs.filter('[name=min_price]').data('min')) {
			delete params.min_price;
		}

		if (params.max_price && params.max_price == $inputs.filter('[name=max_price]').data('max')) {
			delete params.max_price;
		}

		if (!$.isEmptyObject(params)) {
			url += separator + $.param(params, true);
		}

		if ($container.hasClass('layout-carousel')) {
			window.location.href = url;
			return false;
		}

		if (!$container.length) {
			$container = $('<ul class="products"/>');
			$('#main .woocommerce-info').replaceWith($container);
		}

		if (ajax) {
			ajax.abort();
		}

		$form.addClass('filtering');
		$container.addClass('loading').append('<li class="loading-screen"><span class="spinner"></span></li>');

		$(document.body).trigger('konte_products_filter_before_send_request', $container);

		ajax = $.get(url, function (response) {
			var $html = $(response),
				$products = $html.find('ul.products.main-products'),
				$pagination = $container.next('nav'),
				$nav = $html.find('.woocommerce-navigation, .woocomerce-pagination');

			if (!$products.length) {
				$products = $html.find('#main .woocommerce-info');
				$pagination.fadeOut();

				$container.replaceWith($products);
			} else {
				var $nav = $products.next('nav'),
					$order = $('form.woocommerce-ordering');

				if ($nav.length) {
					if ($pagination.length) {
						$pagination.replaceWith($nav).fadeIn();
					} else {
						$container.after($nav);
					}
				} else {
					$pagination.fadeOut();
				}

				$products.children().each(function (index, product) {
					$(product).css('animation-delay', index * 100 + 'ms');
				});

				// Modify the ordering form.
				$inputs.each(function () {
					var $input = $(this),
						name = $input.attr('name'),
						value = $input.val();

					if (name === 'orderby') {
						return;
					}

					if ('min_price' === name && value == $input.data('min')) {
						$order.find('input[name="min_price"]').remove();
						return;
					}

					if ('max_price' === name && value == $input.data('max')) {
						$order.find('input[name="max_price"]').remove();
						return;
					}

					$order.find('input[name="' + name + '"]').remove();

					if (value !== '' && value != 0) {
						$('<input type="hidden" name="' + name + '">').val(value).appendTo($order);
					}
				});

				// Modify the column selector.
				$('.products-toolbar .columns-switcher').replaceWith($html.find('.products-toolbar .columns-switcher'));

				// Replace result count.
				$('.products-toolbar .woocommerce-result-count').replaceWith($html.find('.products-toolbar .woocommerce-result-count'));

				// Replace tabs.
				$('.products-toolbar .products-tabs').replaceWith($html.find('.products-toolbar .products-tabs'));

				$container.replaceWith($products);
				$products.find('li.product').addClass('animated konteFadeInUp');

				$(document.body).trigger('konte_products_loaded', [$products.children(), false]); // appended = false
			}

			$form.removeClass('filtering');
			$(document.body).trigger('konte_products_filter_request_success', [response, url]);
		});
	}
} );
