jQuery( document ).ready( function( $ ) {
	'use strict';

	var wp = window.wp,
		data = window.konte_products_filter_params,
		$body = $( document.body ),
		template = wp.template( 'konte-products-filter' );

	// Toggle instant search checkbox.
	$body.on( 'change', '.konte-products-filter__ajax-option', function() {
		var $this = $( this ),
			checked = $this.is( ':checked' );

		if ( checked ) {
			$this.parent().next( 'p' ).show();
		} else {
			$this.parent().next( 'p' ).hide();
		}
	} );

	// Add new filter.
	$body.on( 'click', '.konte-products-filter-add-new', function( e ) {
		e.preventDefault();

		var $this = $( this ),
			$filters = $this.parent().siblings( '.konte-products-filters' ),
			$title = $filters.closest( '.widget-content' ).find( 'input' ).first();

		data.number = $this.data( 'number' );
		data.name = $this.data( 'name' );
		data.count = $this.data( 'count' );

		$this.data( 'count', data.count + 1 );
		$filters.append( template( data ) );
		$filters.trigger( 'appended' );
		$title.trigger( 'change' ); // Support customize preview.
	} );

	// Toggle display type.
	$body.on( 'change', '.konte-products-filter-fields select.filter-by', function() {
		var $this = $( this ),
			source = $this.val(),
			template = wp.template( 'konte-products-filter-display-options' );

		$this.closest( '.source' ).next( '.display' ).find( 'select.display-type' ).html( template( { options: data.display[source] } ) );

		if ( 'attribute' === source ) {
			$this.next( 'select' ).removeClass( 'hidden' );
			$this.closest( '.source' ).next( '.display' ).find( 'select' ).removeClass( 'hidden' );
		} else {
			$this.next( 'select' ).addClass( 'hidden' );
			$this.closest( '.source' ).next( '.display' ).find( 'select:not(.display-type)' ).addClass( 'hidden' );
		}
	} );

	// Toggle multiple select fields.
	$body.on( 'change', '.konte-products-filter-fields select.display-type', function() {
		var $this = $( this ),
			display = $this.val(),
			source = $this.closest( '.display' ).siblings( '.source' ).find( 'select.filter-by' ).val();

		if ( 'attribute' !== source || 'dropdown' === display ) {
			$this.siblings( 'select.multiple-select, select.query-type' ).addClass( 'hidden' );
		} else {
			$this.siblings( 'select.multiple-select, select.query-type' ).removeClass( 'hidden' );
		}
	} );

	// Toggle query type select field.
	$body.on( 'change', '.konte-products-filter-fields select.multiple-select', function() {
		var $this = $( this ),
			select = $this.val(),
			display = $this.siblings( 'select.display-type' ).val(),
			source = $this.closest( '.display' ).siblings( '.source' ).find( 'select.filter-by' ).val();

		if ( 'attribute' !== source || 'dropdown' === display ) {
			$this.next( 'select.query-type' ).addClass( 'hidden' );
		} else {
			if ( '1' === select ) {
				$this.next( 'select.query-type' ).removeClass( 'hidden' );
			} else {
				$this.next( 'select.query-type' ).addClass( 'hidden' );
			}
		}
	} );

	$body.on( 'click', '.remove-filter', function( e ) {
		e.preventDefault();
		var $filters = $( this ).closest( '.konte-products-filters' ),
			$title = $filters.closest( '.widget-content' ).find( 'input' ).first();

		$( this ).closest( '.konte-products-filter-fields' ).slideUp( 300, function () {
			$( this ).remove();
			$filters.trigger( 'truncated' );
			$title.trigger( 'change' ); // Support customize preview.
		} );
	} );

	$body.on( 'appended truncated', '.konte-products-filters', function() {
		var $filters = $( this ).children( '.konte-products-filter-fields' );

		if ( $filters.length ) {
			$( this ).children( '.no-filter' ).addClass( 'hidden' );
		} else {
			$( this ).children( '.no-filter' ).removeClass( 'hidden' );
		}
	} );
} );
