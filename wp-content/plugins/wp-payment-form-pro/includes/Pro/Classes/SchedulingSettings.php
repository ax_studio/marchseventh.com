<?php

namespace WPPayForm\Pro\Classes;

use WPPayForm\Classes\AccessControl;
use WPPayForm\Classes\ArrayHelper;
use WPPayForm\Classes\Models\Forms;
use WPPayForm\Classes\Models\Submission;

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Form Scheduling and Restriction Class
 * @since 1.0.0
 */
class SchedulingSettings
{
    public function register()
    {
        add_action('wp_ajax_wpf_scheduling_endpoints', array($this, 'schedulingAjaxHandler'));
    }

    public function schedulingAjaxHandler()
    {
        $route = sanitize_text_field($_REQUEST['route']);
        $validRoutes = array(
            'get_settings'    => 'getSettings',
            'update_settings' => 'updateSettings'
        );
        if (isset($validRoutes[$route])) {
            AccessControl::checkAndPresponseError('save_form_settings', 'forms');
            do_action('wppayform/doing_ajax_forms_' . $route);
            return $this->{$validRoutes[$route]}();
        }
    }

    public function getSettings()
    {
        $formId = intval($_REQUEST['form_id']);
        $settings = Forms::getSchedulingSettings($formId);
        wp_send_json_success(array(
            'scheduling_settings' => $settings,
            'current_date_time'   => gmdate('d M Y H:i:s')
        ), 200);
    }

    public function updateSettings()
    {
        $formId = intval($_REQUEST['form_id']);
        $settings = wp_unslash($_REQUEST['settings']);

        if (
            ArrayHelper::get($settings, 'limitNumberOfEntries.status') == 'no' &&
            ArrayHelper::get($settings, 'scheduleForm.status') == 'no' &&
            ArrayHelper::get($settings, 'requireLogin.status') == 'no'
        ) {
            $settings = false;
        }

        update_post_meta($formId, 'wppayform_form_scheduling_settings', $settings);
        wp_send_json_success(array(
            'message' => __('Settings successfully updated', 'wppayform')
        ), 200);
    }

    public function checkRestrictionHooks()
    {
        add_filter('wppayform/form_wrapper_css_classes', array($this, 'checkRestrictionOnRender'), 10, 2);
        add_filter('wppayform/form_submission_validation_errors', array($this, 'validateForm'), 100, 2);
    }

    public function validateForm($errors, $formId)
    {
        if($errors) {
            return $errors;
        }
        if (!get_post_meta($formId, 'wppayform_form_scheduling_settings', true)) {
            return $errors;
        }
        $sheduleSettings = Forms::getSchedulingSettings($formId);
        $errorMessage = '';
        if ($message = $this->checkIfExceedsEntryLimit($formId, $sheduleSettings)) {
            $errorMessage = $message;
        } else if ($timeMessage = $this->checkTimeSchedulingValidityError($formId, $sheduleSettings)) {
            $errorMessage = $timeMessage;
        } else if ($message = $this->checkLoginValidityError($formId, $sheduleSettings)) {
            $errorMessage = $message;
        }
        if($errorMessage) {
            $errors[] = $errorMessage;
        }
        return $errors;
    }

    public function checkRestrictionOnRender($wrapperCSSClasses, $form)
    {
        // if now sheduleing settings found then just return
        if (!get_post_meta($form->ID, 'wppayform_form_scheduling_settings', true)) {
            return $wrapperCSSClasses;
        }
        $extra_css_class = '';
        // We have some schedule settings now so we have add some wrapper class
        $sheduleSettings = $form->scheduleing_settings;
        if ($message = $this->checkIfExceedsEntryLimit($form->ID, $sheduleSettings)) {
            $extra_css_class = 'wpf_exceeds_entry_limit';
            $this->addErrorMessage($form->ID, $message);
        } else if ($timeMessage = $this->checkTimeSchedulingValidityError($form->ID, $sheduleSettings)) {
            $extra_css_class = 'wpf_time_schedule_fail';
            $this->addErrorMessage($form->ID, $timeMessage);
        } else if ($message = $this->checkLoginValidityError($form->ID, $sheduleSettings)) {
            $extra_css_class = 'wpf_logged_in_required';
            $this->addErrorMessage($form->ID, $message);
        }

        if($extra_css_class) {
            $wrapperCSSClasses[]  = $extra_css_class;
            $wrapperCSSClasses[] = 'wpf_restriction_action_'.$sheduleSettings['restriction_applied_type'];
        }
        return $wrapperCSSClasses;
    }

    private function checkIfExceedsEntryLimit($formId, $sheduleSettings)
    {
        if (ArrayHelper::get($sheduleSettings, 'limitNumberOfEntries.status') == 'yes') {
            $limitEntrySettings = ArrayHelper::get($sheduleSettings, 'limitNumberOfEntries');
            $limitPeriod = ArrayHelper::get($limitEntrySettings, 'limit_type');
            $numberOfEntries = ArrayHelper::get($limitEntrySettings, 'number_of_entries');
            $paymentStatuses = ArrayHelper::get($limitEntrySettings, 'limit_payment_statuses');
            $submissionModel = new Submission();
            $totalEntryCount = $submissionModel->getEntryCountByPaymentStatus($formId, $paymentStatuses, $limitPeriod);
            if ($totalEntryCount >= intval($numberOfEntries)) {
                return $limitEntrySettings['limit_exceeds_message']
                    ? $limitEntrySettings['limit_exceeds_message']
                    : __('Submission limit has been excceded.', 'wppayform');
            }
        }
        return false;
    }

    private function checkTimeSchedulingValidityError($formId, $sheduleSettings)
    {
        if (ArrayHelper::get($sheduleSettings, 'scheduleForm.status') == 'yes') {
            $timeSchedule = ArrayHelper::get($sheduleSettings, 'scheduleForm');
            $time = time();
            $start = strtotime($timeSchedule['start_date']);
            $end = strtotime($timeSchedule['end_date']);
            if ($time < $start) {
                return $timeSchedule['before_start_message']
                    ? $timeSchedule['before_start_message']
                    : __('Form submission is not started yet.', 'wppayform');

            }
            if ($time >= $end) {
                return $timeSchedule['expire_message']
                    ? $timeSchedule['expire_message']
                    : __('Form submission is now closed.', 'wppayform');
            }
        }

        return false;
    }

    private function checkLoginValidityError($formId, $sheduleSettings)
    {
        if (ArrayHelper::get($sheduleSettings, 'requireLogin.status') == 'yes') {
            if(!is_user_logged_in()) {
                return  !empty($sheduleSettings['message'])
                    ? $sheduleSettings['message']
                    : __('You must be logged in to submit the form.', 'wppayform');
            }
        }

        return false;
    }

    private function addErrorMessage($formId, $message = '')
    {
        if ($message) {
            add_action('wppayform/form_render_after_' . $formId, function ($form) use ($message) {
                echo '<div class="wpf_form_notices wpf_form_notice_error wpf_form_restrictuon_errors">' . $message . '</div>';
            });
        }
    }
}