<?php
/**
 * Template part for displaying featured post grid
 *
 * @package Konte
 */

$tag                = konte_get_option( 'blog_featured_posts_tag' );
$limit              = konte_get_option( 'blog_featured_posts_limit' );
$featured_post_args = array(
	'post_type'              => 'post',
	'post_status'            => 'publish',
	'posts_per_page'         => intval( $limit ),
	'tag'                    => trim( $tag ),
	'no_found_rows'          => true,
	'update_post_term_cache' => false,
	'update_post_meta_cache' => false,
	'cache_results'          => false,
	'ignore_sticky_posts'    => true,
);

if ( ! $tag || ! $limit ) {
	return;
}

if ( konte_get_option( 'blog_featured_content' ) ) {
	$featured_post_args['post__not_in'] = konte_get_featured_post_ids();
}

$featured_posts = new WP_Query( $featured_post_args );
$css_class      = ( $limit % 3 === 0 ) ? 'col-md-4' : 'col-md-3';

if ( ! $featured_posts->have_posts() ) {
	return;
}
?>

<div id="featured-posts" class="featured-posts">
	<div class="container">
		<h2><?php esc_html_e( 'Featured Posts', 'konte' ) ?></h2>

		<div class="posts row">

			<?php while ( $featured_posts->have_posts() ) : ?>
				<?php $featured_posts->the_post(); ?>

				<div class="post col-sm-6 <?php echo esc_attr( $css_class ); ?>">

					<?php konte_post_thumbnail(); ?>

					<h3 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h3>

					<?php konte_entry_posted_on(); ?>

				</div>

			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>

		</div>

		<hr class="divider" />
	</div>
</div>