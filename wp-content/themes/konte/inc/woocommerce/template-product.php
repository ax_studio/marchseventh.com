<?php
/**
 * Hooks of single product.
 *
 * @package Konte
 */

/**
 * Class of single product template.
 */
class Konte_WooCommerce_Template_Product {
	static $description_tab;

	/**
	 * Initialize.
	 */
	public static function init() {
		// Change header background.
		add_filter( 'konte_header_class', array( __CLASS__, 'header_class' ), 20 );
		add_filter( 'konte_footer_class', array( __CLASS__, 'footer_class' ), 20 );

		// Adds a class of product layout to product page.
		add_filter( 'post_class', array( __CLASS__, 'product_class' ), 10, 3 );

		// Replace the default sale flash.
		remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash' );
		add_action( 'woocommerce_before_single_product_summary', array( __CLASS__, 'badges' ), 10 );

		// Re-order the description.
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
		add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 7 );

		// Remove product tab heading.
		add_filter( 'woocommerce_product_description_heading', '__return_null' );
		add_filter( 'woocommerce_product_reviews_heading', '__return_null' );
		add_filter( 'woocommerce_product_additional_information_heading', '__return_null' );

		// Adds a class of the lightbox to product gallery.
		add_filter( 'woocommerce_single_product_image_gallery_classes', array( __CLASS__, 'gallery_classes' ) );

		// Change lightbox options.
		add_filter( 'woocommerce_single_product_photoswipe_options', array( __CLASS__, 'photoswipe_options' ) );

		// Remore reviewer's avatar.
		remove_action( 'woocommerce_review_before', 'woocommerce_review_display_gravatar' );

		// Change the order of grouped product columns.
		add_filter( 'woocommerce_grouped_product_columns', array( __CLASS__, 'grouped_product_columns' ) );

		// Change related products args.
		add_filter( 'woocommerce_output_related_products_args', array( __CLASS__, 'related_products_args' ) );

		// Re-structure product page.
		switch ( konte_get_option( 'product_layout' ) ) {
			case 'v1':
				// Wrap gallery and summary in a container.
				add_action( 'woocommerce_before_single_product_summary', array( __CLASS__, 'open_gallery_summary_wrapper' ), 19 );
				add_action( 'woocommerce_after_single_product_summary', array( __CLASS__, 'close_gallery_summary_wrapper' ), 1 );

				// Place breadcrumb into product toolbar then place product toolbar before product summary.
				remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
				add_action( 'woocommerce_before_single_product_summary', array( __CLASS__, 'product_toolbar' ), 5 );

				// Change product image carousel options.
				add_filter( 'woocommerce_single_product_carousel_options', array( __CLASS__, 'product_carousel_options' ) );

				// Product sharing.
				add_action( 'woocommerce_after_add_to_cart_button', array( __CLASS__, 'product_share' ), 15 );

				// Move product tabs into the summary.
				remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'product_data_tabs' ), 100 );

				// Place related products outside product container.
				remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
				remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

				add_action( 'woocommerce_after_single_product', 'woocommerce_upsell_display', 10 );
				add_action( 'woocommerce_after_single_product', 'woocommerce_output_related_products', 20 );

				// Support bundle products.
				if ( class_exists( 'WC_Bundles' ) ) {
					add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'reorder_bundle_product_form' ), 90 );
					add_filter( 'woocommerce_bundled_items_grid_layout_columns', array( __CLASS__, 'bundle_product_grid_columns' ) );
				}
				break;

			case 'v2':
				// Change the gallery thumbnail size.
				add_filter( 'woocommerce_gallery_image_size', array( __CLASS__, 'gallery_image_size_large' ) );

				// Place breadcrumb into product toolbar then place product toolbar inside product summary.
				remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'product_toolbar' ), 2 );

				// Remove and add new wishlist button.
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'remove_wishlist_button' ) );
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'add_to_wishlist_button' ), 34 );

				// Product sharing.
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'product_share' ), 35 );

				// Move product tabs into the summary.
				remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'product_data_tabs' ), 100 );

				// Place related products outside product container.
				remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
				remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

				add_action( 'woocommerce_after_single_product', 'woocommerce_upsell_display', 10 );
				add_action( 'woocommerce_after_single_product', 'woocommerce_output_related_products', 20 );

				// Support bundle products.
				if ( class_exists( 'WC_Bundles' ) ) {
					add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'reorder_bundle_product_form' ), 90 );
					add_filter( 'woocommerce_bundled_items_grid_layout_columns', array( __CLASS__, 'bundle_product_grid_columns' ) );
				}
				break;

			case 'v3':
				// Place breadcrumb into product toolbar then place product toolbar before product summary.
				remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
				if ( is_product() ) {
					add_action( 'woocommerce_before_main_content', array( __CLASS__, 'product_toolbar' ), 20 );
				}

				// Change position of product ribbons.
				remove_action( 'woocommerce_before_single_product_summary', array( __CLASS__, 'badges' ), 10 );
				remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'product_title_with_badges' ), 5 );

				// Move product tabs into the summary.
				remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'product_data_tabs' ), 100 );

				// Move add to cart form to another container.
				remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
				add_action( 'woocommerce_after_single_product_summary', array( __CLASS__, 'single_add_to_cart' ), 7 );

				// Change product image carousel options.
				add_filter( 'woocommerce_single_product_carousel_options', array( __CLASS__, 'product_carousel_options' ) );

				// Re-order the stars rating.
				remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
				add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 15 );

				// Product sharing.
				add_action( 'woocommerce_after_add_to_cart_button', array( __CLASS__, 'product_share' ), 15 );

				// Remove related products.
				remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
				remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

				// Support bundle products.
				if ( class_exists( 'WC_Bundles' ) ) {
					remove_action( 'woocommerce_after_single_product_summary', 'wc_pb_template_add_to_cart_after_summary', -1000 );
					add_filter( 'woocommerce_bundled_items_grid_layout_columns', array( __CLASS__, 'bundle_product_grid_columns' ) );
				}
				break;

			case 'v4':
				// Place breadcrumb into product toolbar then place product toolbar inside product summary.
				remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'product_toolbar' ), 2 );

				// Product sharing.
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'product_share' ), 35 );

				// Move product tabs into the summary.
				remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'product_data_tabs' ), 900 );

				// Place product description outside tabs.
				add_filter( 'woocommerce_product_tabs', array( __CLASS__, 'product_v4_tabs' ), 100 );
				add_action( 'woocommerce_after_single_product_summary', array( __CLASS__, 'product_v4_description' ), 25 );

				// Place related products outside product container.
				remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
				remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

				add_action( 'woocommerce_after_single_product', 'woocommerce_upsell_display', 10 );
				add_action( 'woocommerce_after_single_product', 'woocommerce_output_related_products', 20 );

				if ( class_exists( 'WC_Bundles' ) ) {
					// Remove wishlist button for bundled product.
					add_action( 'woocommerce_bundles_add_to_cart_button', array( __CLASS__, 'remove_wishlist_button' ) );
					add_action( 'woocommerce_bundles_add_to_cart_button', array( __CLASS__, 'add_to_wishlist_button' ), 20 );
					add_filter( 'woocommerce_bundled_items_grid_layout_columns', array( __CLASS__, 'bundle_product_grid_columns' ) );
				}
				break;

			case 'v5':
				// Change product image carousel options.
				add_filter( 'woocommerce_single_product_carousel_options', array( __CLASS__, 'product_v5_carousel_options' ) );

				// Wrap gallery and summary in a container.
				add_action( 'woocommerce_before_single_product_summary', array( __CLASS__, 'open_gallery_summary_wrapper' ), 19 );
				add_action( 'woocommerce_after_single_product_summary', array( __CLASS__, 'close_gallery_summary_wrapper' ), 1 );

				// Wrap product summary.
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'product_summary_inner_open' ), 0 );
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'product_summary_inner_close' ), 1000 );
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'product_summary_inner_close' ), 1002 );

				// Move product tabs into the summary.
				remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'product_data_tabs' ), 1001 );

				// Place breadcrumb into product toolbar then place product toolbar inside product summary.
				remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'product_toolbar' ), 2 );

				// Change position of product ribbons.
				remove_action( 'woocommerce_before_single_product_summary', array( __CLASS__, 'badges' ), 10 );
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'badges' ), 4 );

				// Change the gallery thumbnail size.
				add_filter( 'woocommerce_gallery_image_size', array( __CLASS__, 'gallery_image_size_large' ) );

				// Move gallery thumbnail outside.
				// add_action( 'woocommerce_after_single_product_summary', array( __CLASS__, 'product_v5_thumbnails' ), 2 );

				// Remove and add new wishlist button.
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'remove_wishlist_button' ) );
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'add_to_wishlist_button' ), 34 );

				// Product sharing.
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'product_share' ), 35 );

				// Place related products outside product container.
				remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
				remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

				add_action( 'woocommerce_after_single_product', 'woocommerce_upsell_display', 10 );
				add_action( 'woocommerce_after_single_product', 'woocommerce_output_related_products', 20 );

				// Support bundle products.
				if ( class_exists( 'WC_Bundles' ) ) {
					add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'reorder_bundle_product_form' ), 90 );
					add_filter( 'woocommerce_bundled_items_grid_layout_columns', array( __CLASS__, 'bundle_product_grid_columns' ) );
				}
				break;

			case 'v6':
				// Place breadcrumb into product toolbar then place product toolbar inside product summary.
				remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'product_toolbar' ), 2 );

				// Remove and add new wishlist button.
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'remove_wishlist_button' ) );
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'add_to_wishlist_button' ), 34 );

				// Product sharing.
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'product_share' ), 35 );

				// Place related products outside product container.
				remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
				remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

				add_action( 'woocommerce_after_single_product', 'woocommerce_upsell_display', 10 );
				add_action( 'woocommerce_after_single_product', 'woocommerce_output_related_products', 20 );

				// Support bundle products.
				if ( class_exists( 'WC_Bundles' ) ) {
					add_filter( 'woocommerce_bundled_items_grid_layout_columns', array( __CLASS__, 'bundle_product_grid_columns' ) );
				}
				break;

			case 'v7':
				// Place breadcrumb into product toolbar then place product toolbar inside product summary.
				remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'product_toolbar' ), 2 );

				// Add a section for related products on the right side.
				add_action( 'woocommerce_after_single_product_summary', array( __CLASS__, 'side_products' ), 5 );

				// Remove and add new wishlist button.
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'remove_wishlist_button' ) );
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'add_to_wishlist_button' ), 34 );

				// Product sharing.
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'product_share' ), 35 );

				// Place related products outside product container.
				remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
				remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

				add_action( 'woocommerce_after_single_product', 'woocommerce_upsell_display', 10 );
				add_action( 'woocommerce_after_single_product', 'woocommerce_output_related_products', 20 );

				// Support bundle products.
				if ( class_exists( 'WC_Bundles' ) ) {
					remove_action( 'woocommerce_after_single_product_summary', 'wc_pb_template_add_to_cart_after_summary', -1000 );
					add_action( 'woocommerce_after_single_product_summary', 'wc_pb_template_add_to_cart_after_summary', 7 );
					add_filter( 'woocommerce_bundled_items_grid_layout_columns', array( __CLASS__, 'bundle_product_grid_columns' ) );
				}
				break;
		}
	}

	/**
	 * Make header transparent on product page if selected product layout is Version 1.
	 *
	 * @param array $classes Header classes.
	 *
	 * @return array
	 */
	public static function header_class( $classes ) {
		if ( ! is_product() ) {
			return $classes;
		}

		if ( in_array( konte_get_option( 'product_layout' ), array( 'v1', 'v3', 'v5' ) ) ) {
			$classes = array_diff( $classes, array( 'dark', 'light', 'custom', 'text-dark', 'text-light' ) );

			$classes[] = 'transparent';
			$classes[] = 'text-dark';
		} elseif ( in_array( 'transparent', $classes ) ) {
			$classes = array_diff( $classes, array( 'dark', 'transparent', 'custom', 'text-dark', 'text-light' ) );
			$classes[] = 'light';
			$classes[] = 'text-dark';
		}

		return $classes;
	}

	/**
	 * Make footer be transparent on product page v3
	 *
	 * @param array $classes Footer classes.
	 *
	 * @return array
	 */
	public static function footer_class( $classes ) {
		if ( ! is_product() ) {
			return $classes;
		}

		if ( 'v3' == konte_get_option( 'product_layout' ) ) {
			$classes = array_diff( $classes, array( 'dark', 'light', 'custom', 'text-dark', 'text-light' ) );

			$classes[] = 'transparent';
			$classes[] = 'text-dark';
		}

		return $classes;
	}

	/**
	 * Adds classes to products
	 *
	 * @param array  $classes Post classes.
	 * @param string $class   Post class.
	 * @param string $post_id Post ID.
	 *
	 * @return array
	 */
	public static function product_class( $classes, $class = '', $post_id = '' ) {
		if ( is_admin() && ! defined( 'DOING_AJAX' ) ) {
			return $classes;
		}

		$post_type = get_post_type( $post_id );

		// Ignore other post types.
		if ( ! in_array( $post_type, array( 'product', 'product_variation' ) ) ) {
			return $classes;
		}

		if ( is_single( $post_id ) && 'product' == $post_type ) {
			global $product;

			$classes[] = 'layout-' . konte_get_option( 'product_layout' );
			$classes[] = 'clearfix';

			if ( in_array( konte_get_option( 'product_layout' ), array(
					'v1',
					'v3',
				) ) && get_post_meta( $post_id, 'background_color', true ) ) {
				$classes[] = 'background-set';
			}

			if ( ! $product->get_gallery_image_ids() ) {
				$classes[] = 'empty-gallery';
			}
		}

		return $classes;
	}

	/**
	 * Product badges.
	 */
	public static function badges() {
		$badges = self::get_badges();

		if ( $badges ) {
			$shape = konte_get_option( 'shop_badge_shape' );
			printf( '<span class="woocommerce-badges woocommerce-badges--%s">%s</span>', esc_attr( $shape ), implode( '', $badges ) );
		}
	}

	/**
	 * Get product badges.
	 *
	 * @return array
	 */
	public static function get_badges() {
		global $product;

		$badges = array();

		if ( $product->is_on_sale() && konte_get_option( 'shop_badge_sale' ) ) {
			ob_start();
			woocommerce_show_product_sale_flash();
			$badges['sale'] = ob_get_clean();
		}

		if ( $product->is_featured() && konte_get_option( 'shop_badge_featured' ) ) {
			$text               = konte_get_option( 'shop_badge_featured_text' );
			$text               = empty( $text ) ? esc_html__( 'Hot', 'konte' ) : $text;
			$badges['featured'] = '<span class="featured woocommerce-badge"><span>' . esc_html( $text ) . '</span></span>';
		}

		if ( konte_get_option( 'shop_badge_new' ) && in_array( $product->get_id(), konte_woocommerce_get_new_product_ids() ) ) {
			$text          = konte_get_option( 'shop_badge_new_text' );
			$text          = empty( $text ) ? esc_html__( 'New', 'konte' ) : $text;
			$badges['new'] = '<span class="new woocommerce-badge"><span>' . esc_html( $text ) . '</span></span>';
		}

		if ( konte_get_option( 'shop_badge_soldout' ) && ! $product->is_in_stock() ) {
			$in_stock = false;

			// Double check if this is a variable product.
			if ( $product->is_type( 'variable' ) ) {
				$variations = $product->get_available_variations();

				foreach ( $variations as $variation ) {
					if( $variation['is_in_stock'] ) {
						$in_stock = true;
						break;
					}
				}
			}

			if ( ! $in_stock ) {
				$text               = konte_get_option( 'shop_badge_soldout_text' );
				$text               = empty( $text ) ? esc_html__( 'Sold Out', 'konte' ) : $text;
				$badges['sold-out'] = '<span class="sold-out woocommerce-badge"><span>' . esc_html( $text ) . '</span></span>';
			}
		}

		$badges = apply_filters( 'konte_product_badges', $badges, $product );
		ksort( $badges );

		return $badges;
	}

	/**
	 * Adds custom classes to product image gallery
	 *
	 * @param array $classes Gallery classes.
	 *
	 * @return array
	 */
	public static function gallery_classes( $classes ) {
		global $product;

		if ( current_theme_supports( 'wc-product-gallery-lightbox' ) ) {
			$classes[] = 'lightbox-support';
		}

		if ( current_theme_supports( 'wc-product-gallery-zoom' ) ) {
			$classes[] = 'zoom-support';
		}

		$attachment_ids = $product->get_gallery_image_ids();

		if ( ! $attachment_ids ) {
			$classes[] = 'no-thumbnails';
		}

		return $classes;
	}

	/**
	 * Changes photoswipe options
	 *
	 * @param array $options Photoswipe script options.
	 *
	 * @return array
	 */
	public static function photoswipe_options( $options ) {
		$options['captionEl']             = false;
		$options['showHideOpacity']       = true;
		$options['showAnimationDuration'] = 400;
		$options['hideAnimationDuration'] = 400;

		return $options;
	}

	/**
	 * Re-order grouped product
	 *
	 * @param array $columns Columns of fields.
	 *
	 * @return array
	 */
	public static function grouped_product_columns( $columns ) {
		$key = array_search( 'label', $columns );

		if ( false !== $key ) {
			$label = $columns[ $key ];
			unset( $columns[ $key ] );
			array_unshift( $columns, $label );
		}

		return $columns;
	}

	/**
	 * Increase amount of related products to create carousel for it.
	 *
	 * @param array $args Related products query args.
	 *
	 * @return array
	 */
	public static function related_products_args( $args ) {
		$args['posts_per_page'] = $args['columns'] * 3;

		return $args;
	}

	/**
	 * Open gallery summary wrapper
	 */
	public static function open_gallery_summary_wrapper() {
		$container = '';

		if ( 'v1' == konte_get_option( 'product_layout' ) ) {
			$container = 'konte-container';
		}

		echo '<div class="product-gallery-summary clearfix ' . esc_attr( $container ) . '">';
	}

	/**
	 * Close gallery summary wrapper
	 */
	public static function close_gallery_summary_wrapper() {
		echo '</div>';
	}

	/**
	 * Displays product toolbar
	 */
	public static function product_toolbar() {
		$breadcrumb = konte_get_option( 'product_breadcrumb' );
		$navigation = konte_get_option( 'product_navigation' );

		if ( ! $breadcrumb && ! $navigation ) {
			return;
		}

		if ( ! $breadcrumb && in_array( konte_get_option( 'product_layout' ), array( 'v6', 'v7' ) ) ) {
			return;
		}
		?>
		<div class="product-toolbar clearfix">
			<?php
			if ( $breadcrumb ) {
				Konte_Breadcrumbs::breadcrumbs();
			}

			if ( $navigation ) {
				$nav_args = array(
					'prev_text'          => esc_html__( 'Prev', 'konte' ),
					'next_text'          => esc_html__( 'Next', 'konte' ),
					'screen_reader_text' => esc_html__( 'Product navigation', 'konte' ),
				);

				if ( konte_get_option( 'product_navigation_same_cat' ) ) {
					$nav_args['in_same_term'] = true;
					$nav_args['taxonomy'] = 'product_cat';
				}

				the_post_navigation( $nav_args );
			}
			?>
		</div>
		<?php
	}

	/**
	 * Changes flex slider options
	 *
	 * @param array $options Gallery carousel options.
	 *
	 * @return array
	 */
	public static function product_carousel_options( $options ) {
		$options['animation']  = 'fade';
		$options['controlNav'] = true;

		return $options;
	}

	/**
	 * Changes flex slider options for product v5.
	 *
	 * @param array $options Gallery carousel options.
	 *
	 * @return array
	 */
	public static function product_v5_carousel_options( $options ) {
		$options['controlNav'] = true;

		return $options;
	}

	/**
	 * Displays sharing buttons.
	 */
	public static function product_share() {
		if ( ! function_exists( 'konte_addons_share_link' ) ) {
			return;
		}

		if ( ! konte_get_option( 'product_sharing' ) ) {
			return;
		}

		$socials = konte_get_option( 'product_sharing_socials' );
		$whatsapp_number = konte_get_option( 'product_sharing_whatsapp_number' );

		if ( empty( $socials ) ) {
			return;
		}
		?>
		<div class="product-share share">
			<span class="svg-icon icon-socials sharing-icon">
				<svg><use href="#socials" xlink:href="#socials"></svg>
				<span><?php esc_html_e( 'Share', 'konte' ) ?></span>
			</span>
			<span class="socials">
				<?php
				foreach ( $socials as $social ) {
					echo konte_addons_share_link( $social, array( 'whatsapp_number' => $whatsapp_number ) );
				}
				?>
			</span>
		</div>
		<?php
	}

	/**
	 * Product data tabs.
	 */
	public static function product_data_tabs() {
		/**
		 * Filter tabs and allow third parties to add their own.
		 *
		 * Each tab is an array containing title, callback and priority.
		 *
		 * @see woocommerce_default_product_tabs()
		 */
		$tabs = apply_filters( 'woocommerce_product_tabs', array() );

		if ( ! empty( $tabs ) ) :
			?>

			<div class="woocommerce-tabs wc-tabs-wrapper panels-offscreen">
				<ul class="tabs wc-tabs" role="tablist">
					<?php foreach ( $tabs as $key => $tab ) : ?>
						<li class="<?php echo esc_attr( $key ); ?>_tab" id="tab-title-<?php echo esc_attr( $key ); ?>" role="tab" aria-controls="tab-<?php echo esc_attr( $key ); ?>">
							<a href="#tab-<?php echo esc_attr( $key ); ?>"><?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', esc_html( $tab['title'] ), $key ); ?></a>
						</li>
					<?php endforeach; ?>
				</ul>
				<div class="panels">
					<div class="backdrop"></div>
					<?php foreach ( $tabs as $key => $tab ) : ?>
						<div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--<?php echo esc_attr( $key ); ?> panel entry-content wc-tab" id="tab-<?php echo esc_attr( $key ); ?>" role="tabpanel" aria-labelledby="tab-title-<?php echo esc_attr( $key ); ?>">
							<div class="hamburger-menu button-close active">
								<span class="menu-text"><?php esc_html_e( 'Close', 'konte' ) ?></span>

								<div class="hamburger-box">
									<div class="hamburger-inner"></div>
								</div>
							</div>
							<div class="panel-header">
								<h3><?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', esc_html( $tab['title'] ), $key ); ?></h3>
							</div>
							<div class="panel-content">
								<?php if ( isset( $tab['callback'] ) ) {
									call_user_func( $tab['callback'], $key, $tab );
								} ?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>

		<?php
		endif;
	}

	/**
	 * Change the image size of product v2.
	 *
	 * @param string $size Image size name.
	 *
	 * @return string
	 */
	public static function gallery_image_size_large( $size ) {
		return 'woocommerce_single';
	}

	/**
	 * Remove add-to-wishlist button.
	 */
	public static function remove_wishlist_button() {
		if ( ! is_product() || ! class_exists( 'Soo_Wishlist_Frontend' ) ) {
			return;
		}

		remove_action( 'woocommerce_after_add_to_cart_button', array(
			'Soo_Wishlist_Frontend',
			'single_product_button',
		) );
	}

	/**
	 * Display product title with the badges.
	 */
	public static function product_title_with_badges() {
		echo '<h1 class="product_title entry-title">';
		the_title();
		echo implode( '', self::get_badges() );
		echo '</h1>';
	}

	/**
	 * Product cart form.
	 */
	public static function single_add_to_cart() {
		echo '<div class="woocommerce-product-cart">';
		woocommerce_template_single_add_to_cart();
		echo '</div>';
	}

	/**
	 * Remove description for product tabs on product layout v4
	 *
	 * @param array $tabs Product tabs.
	 *
	 * @return array
	 */
	public static function product_v4_tabs( $tabs ) {
		if ( ! isset( $tabs['description'] ) ) {
			return $tabs;
		}

		self::$description_tab = $tabs['description'];
		unset( $tabs['description'] );

		return $tabs;
	}

	/**
	 * Place product descritpion outside on product layout v4
	 */
	public static function product_v4_description() {
		if ( ! isset( self::$description_tab ) ) {
			return;
		}

		echo '<div class="woocommerce-product-details__description product-description">';
		call_user_func( self::$description_tab['callback'], 'description', self::$description_tab );
		echo '</div>';
	}

	/**
	 * Open product summary inner.
	 */
	public static function product_summary_inner_open() {
		echo '<div class="summary-inner product-summary-inner"><div class="product-summary">';
	}

	/**
	 * Close product summary inner.
	 */
	public static function product_summary_inner_close() {
		echo '</div>';
	}

	/**
	 * Product thumbnails for product layout v5.
	 */
	public static function product_v5_thumbnails() {
		$lightbox_class = konte_get_option( 'product_image_lightbox' ) ? 'lightbox-support' : '';

		echo '<div class="product-gallery-thumbnails ' . esc_attr( $lightbox_class ) . '">';
		woocommerce_show_product_thumbnails();
		echo '</div>';
	}

	/**
	 * Display side products on prduct page v7.
	 */
	public static function side_products() {
		if ( ! class_exists( 'WC_Shortcode_Products' ) ) {
			return;
		}

		global $product;

		$limit = konte_get_option( 'product_side_products_limit' );
		$type  = konte_get_option( 'product_side_products' );

		if ( 'related_products' == $type ) {
			$query = new stdClass();

			$related_products = array_filter( array_map( 'wc_get_product', wc_get_related_products( $product->get_id(), $limit, $product->get_upsell_ids() ) ), 'wc_products_array_filter_visible' );
			$related_products = wc_products_array_orderby( $related_products, 'rand', 'desc' );

			$query->posts = $related_products;
		} else {
			$atts  = array(
				'per_page'     => intval( $limit ),
				'category'     => '',
				'cat_operator' => 'IN',
			);

			switch ( $type ) {
				case 'sale_products':
				case 'top_rated_products':
					$atts = array_merge( array(
						'orderby' => 'title',
						'order'   => 'ASC',
					), $atts );
					break;

				case 'recent_products':
					$atts = array_merge( array(
						'orderby' => 'date',
						'order'   => 'DESC',
					), $atts );
					break;

				case 'featured_products':
					$atts = array_merge( array(
						'orderby' => 'date',
						'order'   => 'DESC',
						'orderby' => 'rand',
					), $atts );
					$atts['visibility'] = 'featured';
					break;
			}

			$args  = new WC_Shortcode_Products( $atts, $type );
			$args  = $args->get_query_args();
			$query = new WP_Query( $args );
		}

		echo '<div class="side-products">';
		echo '	<h2>' . esc_html( konte_get_option( 'product_side_products_title' ) ) . '</h2>';
		echo '	<ul class="products ' . esc_attr( str_replace( '_', '-', $type ) ) . '">';

		foreach ( $query->posts as $product_id ) {
			$_product = is_numeric( $product_id ) ? wc_get_product( $product_id ) : $product_id;
			?>

			<li>
				<a href="<?php echo esc_url( $_product->get_permalink() ); ?>">
					<?php echo wp_kses_post( $_product->get_image( 'woocommerce_gallery_thumbnail' ) ); ?>
					<span class="product-info">
						<span class="product-title"><?php echo wp_kses_post( $_product->get_name() ); ?></span>
						<span class="product-price"><?php echo wp_kses_post( $_product->get_price_html() ); ?></span>
					</span>
				</a>
			</li>

			<?php
		}

		wp_reset_postdata();

		echo '	</ul>';
		echo '</div>';
	}

	/**
	 * Add to wishlist button
	 */
	public static function add_to_wishlist_button() {
		if ( shortcode_exists( 'add_to_wishlist' ) ) {
			echo do_shortcode( '[add_to_wishlist]' );
		}
	}

	public static function reorder_bundle_product_form() {
		global $product;

		if ( ! function_exists( 'wc_pb_is_product_bundle' ) ) {
			return;
		}

		if ( ! wc_pb_is_product_bundle() ) {
			return;
		}

		if ( 'after_summary' != $product->get_add_to_cart_form_location() ) {
			return;
		}

		remove_action( 'woocommerce_after_single_product_summary', 'wc_pb_template_add_to_cart_after_summary', -1000 );

		add_filter( 'woocommerce_product_get_add_to_cart_form_location', array( __CLASS__, 'add_to_cart_form_location_default' ) );
		wc_pb_template_add_to_cart();
		remove_filter( 'woocommerce_product_get_add_to_cart_form_location', array( __CLASS__, 'add_to_cart_form_location_default' ) );
	}

	public static function add_to_cart_form_location_default() {
		return 'default';
	}

	public static function bundle_product_grid_columns() {
		global $product;

		$product_layout = konte_get_option( 'product_layout' );

		if ( in_array( $product_layout, array( 'v4', 'v6' ) ) ) {
			return 4;
		} elseif ( 'v7' == $product_layout && 'after_summary' == $product->get_add_to_cart_form_location() ) {
			return 3;
		}

		return 2;
	}
}
